<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class GameController extends Controller
{
    //
    public function index()
    {
        $games = DB::table('table_game')->get();
        return view('game.index', compact('games'));
    }
    public function create()
    {
        return view('game.create');
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year'      => 'required|integer',
            ],
            [            
                'name.required'  => 'kolom name harus diisi',
                'gameplay.required'  => 'kolom gameplay harus diisi',
                'developer.required'  => 'kolom Developer harus diisi',
                'year.required'  => 'kolom year harus diisi',
                'year.integer'  => 'kolom year harus diisi dengan angka',
            ]
        );
        DB::table('table_game')->insert(
            [
                'name'  => $request['name'],
                'gameplay'  => $request['gameplay'],
                'developer'  => $request['developer'],
                'year'  => $request['year']
            ]
        );
        return redirect('/game');
    }
    public function show($id)
    {
        $game =  DB::table('table_game')->where('id', $id)->first();
        return view('game.show', compact('game'));
    }
    public function edit($id)
    {
        $game =  DB::table('table_game')->where('id', $id)->first();
        return view('game.edit', compact('game'));
    }
    public function update($id, Request $request)
    {
        $request->validate(
            [
                'name' => 'required',
                'gameplay' => 'required',
                'developer' => 'required',
                'year'      => 'required|integer',
            ],
            [            
                'name.required'  => 'kolom name harus diisi',
                'gameplay.required'  => 'kolom gameplay harus diisi',
                'developer.required'  => 'kolom Developer harus diisi',
                'year.required'  => 'kolom year harus diisi',
                'year.integer'  => 'kolom year harus diisi dengan angka',
            ]
        );
        DB::table('table_game')->where('id', $id)
        ->update(
            [
                'name'  => $request['name'],
                'gameplay'  => $request['gameplay'],
                'developer'  => $request['developer'],
                'year'  => $request['year'],
            ]
            );

            return redirect('/game');
    }
    public function destroy($id)
    {
        DB::table('table_game')->where('id', '=', $id)->delete();
        return redirect('/game');
    }
}
