<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;
use App\Berita;
use Illuminate\Support\Facades\File;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $berita = Berita::all();
        return view('berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $kategori = Kategori::all();
        return view('berita.create', compact('kategori'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate(
            [
                'judul' => 'required',
                'content' => 'required',
                'kategori_id' => 'required',
                'thumbnail'     => 'required|mimes:jpg,jpeg,png|max:2200',
            ],
            [            
                'judul.required'  => 'kolom judul harus diisi',
                'content.required'  => 'kolom content harus diisi',
                'kategori_id.required'  => 'kolom kategori_id harus diisi',
                'thumbnail.required'  => 'kolom thumbnail harus diisi',
                'thumbnail.mimes'  => 'Thumbnail harus berekstensi jpg/jpeg/png',
                'thumbnail.max'  => 'Thumbnail sizes maks 2200',
            ]
        );
        $gambar = $request->thumbnail;
        $new_gambar = time() . '-' . $gambar->getClientOriginalName();

        $berita = new Berita;
        $berita->judul          = $request->judul;
        $berita->content        = $request->content;
        $berita->kategori_id    =   $request->kategori_id;
        $berita->thumbnail      =   'thumbnail/'.$new_gambar;
        $berita->save();
        $gambar->move('thumbnail/', $new_gambar);
        return redirect('/berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $berita = Berita::findOrFail($id);
        return view('berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $berita = Berita::findOrFail($id);
        $kategori   = Kategori::all();

        return view('berita.edit', compact('berita', 'kategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->validate(
            [
                'judul' => 'required',
                'content' => 'required',
                'kategori_id' => 'required',
                'thumbnail'     => 'mimes:jpg,jpeg,png|max:2200',
            ],
            [            
                'judul.required'  => 'kolom judul harus diisi',
                'content.required'  => 'kolom content harus diisi',
                'kategori_id.required'  => 'kolom kategori_id harus diisi',
                'thumbnail.mimes'  => 'Thumbnail harus berekstensi jpg/jpeg/png',
                'thumbnail.max'  => 'Thumbnail sizes maks 2200',
            ]
        );
        $berita = Berita::find($id);

        if ($request->has('thumbnail'))
        {
            $path   = "thumbnail/";
            File::delete($berita->thumbnail);
            $gambar = $request->thumbnail;
            $new_gambar =  time(). '-' . $gambar->getClientOriginalName();
            $gambar->move('thumbnail/', $new_gambar);
            $berita->thumbnail = 'thumbnail/'.$new_gambar;
        }

        $berita->judul          = $request->judul;
        $berita->content        = $request->content;
        $berita->kategori_id    =   $request->kategori_id;
        $berita->save();
        
        return redirect('/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $berita = Berita::find($id);
        File::delete($berita->thumbnail);
        $berita->delete();
        return redirect('/berita');
    }
}
