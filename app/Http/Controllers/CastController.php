<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    //
    public function index()
    {
        $casts = DB::table('table_cast')->get();

        return view('casts.index', compact('casts'));
    }
    public function create()
    {
        return view('casts.create');
    }
    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|integer',
                'bio' => 'required',
            ],
            [            
                'nama.required'  => 'kolom nama harus diisi',
                'umur.required'  => 'kolom umur harus diisi',
                'umur.integer'  => 'kolom umur harus diisi dengan angka',
                'bio.required'  => 'kolom bio harus diisi',
            ]
        );
        DB::table('table_cast')->insert(
            [
                'nama'  => $request['nama'],
                'umur'  => $request['umur'],
                'bio'  => $request['bio'],
            ]
        );
        return redirect('/cast');
    }
    public function show($id)
    {
        $cast =  DB::table('table_cast')->where('id', $id)->first();
        return view('casts.show', compact('cast'));
    }
    public function edit($id)
    {
        $cast =  DB::table('table_cast')->where('id', $id)->first();
        return view('casts.edit', compact('cast'));
    }
    public function update($id, Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required|integer',
                'bio' => 'required',
            ],
            [            
                'nama.required'  => 'kolom nama harus diisi',
                'umur.required'  => 'kolom umur harus diisi',
                'umur.integer'  => 'kolom umur harus diisi dengan angka',
                'bio.required'  => 'kolom bio harus diisi',
            ]
        );
        DB::table('table_cast')->where('id', $id)
        ->update(
            [
                'nama'  => $request['nama'],
                'umur'  => $request['umur'],
                'bio'  => $request['bio'],

            ]
            );

            return redirect('/cast');
    }
    public function destroy($id)
    {
        DB::table('table_cast')->where('id', '=', $id)->delete();
        return redirect('/cast');
    }
}
