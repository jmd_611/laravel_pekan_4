<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    //
    protected $table = "b_berita";
    protected $fillable = [
        "judul", "content", "thumbnail", "kategori_id"
    ];
}
