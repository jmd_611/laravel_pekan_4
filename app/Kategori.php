<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    //
    protected $table = "b_kategori";
    protected $fillable = [
        "nama", "deskripsi"
    ];
}
