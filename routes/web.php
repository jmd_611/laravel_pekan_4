<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Http\Controllers\CastController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\KategoriController;
use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/data/table', function(){
    return view('template.table');
});

Route::get('/data/data-table', function(){
    return view('template.datatable');
});

Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');
Route::delete('/cast/{cast_id}', 'CastController@destroy');


Route::get('/game', 'GameController@index');
Route::get('/game/create', 'GameController@create');
Route::post('/game', 'GameController@store');
Route::get('/game/{game_id}', 'GameController@show');
Route::get('/game/{game_id}/edit', 'GameController@edit');
Route::put('/game/{game_id}', 'GameController@update');
Route::delete('/game/{game_id}', 'GameController@destroy');

Route::resource('berita', 'BeritaController');
Route::resource('/kategori', "KategoriController");
