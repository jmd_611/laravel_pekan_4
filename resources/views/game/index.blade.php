@extends('adminlte.master')

@section('judul')
Halaman Index Game
@endsection

@section('content')
<div class="card-body">
  <div class="card">
    <div class="card card-primary">
      <div class="card-header">
      </div>
      <!-- /.card-header -->
    <!-- /.card-header -->
    <div class="card-body">
      <a href="/game/create" class="btn btn-primary my-3">Tambah Data game</a>
      <table id="game" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
          <th>No</th>
          <th>Name</th>
          <th>Game Play</th>
          <th>Developer</th>
          <th>Year</th>
          <th style="text-align:center; vertical-align: inherit; width: 150px;">Action</th>
        </tr>
        </thead>
        <tbody>
          @forelse ($games as $key => $item)
              <tr>
                <td>{{ $key + 1}}</td>
                <td>{{ $item->name}}</td>
                <td>{{ $item->gameplay}}</td>
                <td>{{$item->developer}}</td>
                <td>{{$item->year}}</td>
                <td >
                  <form action="/game/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/game/{{$item->id}}" class="btn btn-info btn-sm"><i class="fa fa-info-circle" aria-hidden="true"></i> </a> 
                    <a href="/game/{{$item->id}}/edit " class="btn btn-warning btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> </a>
                    <button type="submit" class="btn btn-danger btn-sm">
                      <i class="fa fa-trash fa-sm"></i>
                  </button>
                  </form>
                </td>
              </tr>
          @empty
              <h1>Data Lagi Kosong</h1>
          @endforelse
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>
@endsection