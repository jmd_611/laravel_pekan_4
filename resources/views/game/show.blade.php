@extends('adminlte.master')

@section('judul')
Halaman Show Game
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
              </div>
              <!-- /.card-header -->
              <!-- form start -->
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" disabled value="{{$game->name}}">
                  </div>
                  <div class="form-group">
                    <label for="gameplay">Game Play</label>
                    <input type="text" name="gameplay" class="form-control" id="gameplay" disabled value="{{$game->gameplay}}">
                  </div>
                  <div class="form-group">
                    <label for="developer">Developer</label>
                    <input type="text" name="developer" class="form-control" id="developer" disabled value="{{$game->developer}}">
                  </div>
                  <div class="form-group">
                    <label for="year">year</label>
                    <input type="text" name="year" class="form-control" id="year" disabled value="{{$game->year}}">
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <a href="/game" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                </div>
            </div>
            <!-- /.card -->

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection