@extends('adminlte.master')

@section('judul')
Halaman Create Game
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/game" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Name</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="Enter name">
                  </div>
                  @error('name')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                  <div class="form-group">
                    <label for="gameplay">Game Play</label>
                    <input type="text" name="gameplay" class="form-control" id="gameplay" placeholder="gameplay">
                  </div>
                  @error('gameplay')
                    <div class="alert alert-danger">{{ $message}}</div>
                   @enderror
                  <div class="form-group">
                    <label for="developer">Developer</label>
                    <input type="text" name="developer" class="form-control" id="developer" placeholder="developer">
                  </div>
                  @error('developer')
                  <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                  <div class="form-group">
                    <label for="year">Year</label>
                    <input type="text" name="year" class="form-control" id="year" placeholder="year">
                  </div>
                  @error('year')
                  <div class="alert alert-danger">{{ $message}}</div>
                  @enderror

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <a href="/cast" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection