@extends('adminlte.master')

@section('judul')
Halaman List Berita
@endsection

@section('content')

<div class="card-body">
  
</div>

<div class="card-body">
  <div class="card">
      <div class="card-header">
        <a href="/berita/create" class="btn btn-success my-2">Tambah Berita</a>
      </div>
      <!-- /.card-header -->
    <!-- /.card-header -->
    <div class="card-body">
      <div class="row">
        @foreach ($berita as $item)
          <div class="col-4">
            <div class="card">
              <img src="{{ url($item->thumbnail, []) }}" class="card-img-top" alt="">
                <div class="card-body">
                  <h3 class="card-title">{{ Str::upper($item->judul) }}</h3>
                  <p class="card-text">{{ Str::limit($item->content, 20) }}</p>
                  <form action="berita/{{ $item->id }}" method="POST">
                    @method('delete')
                    @csrf
                    <a href="berita/{{ $item->id }}" class="btn btn-info btn-sm">Detail</a>
                    <a href="berita/{{ $item->id }}/edit" class="btn btn-warning btn-sm">Edit</a>
                    <input type="submit" value="Delete" class="btn btn-danger btn-sm">
                  </form>
                </div>
            </div>
          </div>
            
        @endforeach
      </div>
    </div>
    <!-- /.card-body -->
  </div>
</div>

@endsection