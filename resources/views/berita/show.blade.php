@extends('adminlte.master')

@section('judul')
Halaman Detail Berita
@endsection

@section('content')

<div class="card-body">
  
</div>

<div class="card-body">
  <div class="card">
      <div class="card-header">
        <a href="/berita" class="btn btn-info my-2">Kembali</a>
      </div>
    <div class="card-body">
      <div class="row">
          <div class="col-12">
            <div class="card">
              <img src="{{ url($berita->thumbnail, []) }}" height="300px" width="300px" alt="">
                <h3 class="border-primary">{{ Str::upper($berita->judul) }}</h3>
                <p class="">{{ $berita->content }}</p>
            </div>
          </div>
      </div>
    </div>
  </div>
</div>

@endsection