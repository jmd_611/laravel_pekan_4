@extends('adminlte.master')

@section('judul')
Halaman Tambah Data Berita
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/berita" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="kategori">Kategori</label>
                    <select name="kategori_id" id="" class="form-control">
                      <option value="" disabled>--Pilih kategori--</option>
                      @foreach ($kategori as $item)
                        <option value="{{$item->id}}">{{ $item->nama}}</option>
                      @endforeach
                    </select>
                  </div>
                  @error('kategori_id')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                  <div class="form-group">
                    <label for="judul">judul</label>
                    <input type="text" name="judul" class="form-control" id="judul" placeholder="Enter judul">
                  </div>
                  @error('judul')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                  <div class="form-group">
                    <label for="content">Content</label>
                    <textarea name="content" id="" cols="content" rows="5" class="form-control" id="content" placeholder="isi dengan content berita"></textarea>
                  </div>
                  @error('content')
                  <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                  <div class="form-group">
                    <label for="thumbnail">Thumbnail</label>
                    <input type="file" name="thumbnail" class="form-control" id="thumbnail" placeholder="Chose File">
                  </div>
                  @error('thumbnail')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <a href="/cast" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@push('scripts')
  <script>

  </script>
@endpush