@extends('adminlte.master')

@section('judul')
Halaman Daftar Data Cast
@endsection

@section('content')
<div class="card-body">
  <div class="card">
    <div class="card card-primary">
      <div class="card-header">
      </div>
      <!-- /.card-header -->
    <!-- /.card-header -->
    <div class="card-body">
      <a href="/cast/create" class="btn btn-primary my-3">Tambah Data Cast</a>
      <table id="cast" class="table table-striped table-bordered" style="width:100%">
        <thead>
        <tr>
          <th>No</th>
          <th>Nama</th>
          <th>Umur</th>
          <th>Biografi</th>
          <th style="text-align:center; vertical-align: inherit; width: 150px;">Action</th>
        </tr>
        </thead>
        <tbody>
          @forelse ($casts as $key => $item)
              <tr>
                <td>{{ $key + 1}}</td>
                <td>{{ $item->nama}}</td>
                <td>{{ $item->umur}}</td>
                <td>{{$item->bio}}</td>
                <td >
                  <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    @method('delete')
                    <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm"><i class="fa fa-info-circle" aria-hidden="true"></i> </a> 
                    <a href="/cast/{{$item->id}}/edit " class="btn btn-warning btn-sm"><i class="fa fa-edit" aria-hidden="true"></i> </a>
                    <button type="submit" class="btn btn-danger btn-sm">
                      <i class="fa fa-trash fa-sm"></i>
                  </button>
                  </form>
                </td>
              </tr>
          @empty
              <h1>Data Lagi Kosong</h1>
          @endforelse
      </table>
    </div>
    <!-- /.card-body -->
  </div>
</div>
@endsection

@push('scripts')
<script src="{{asset('adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
<script src="{{asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
<script>
  $(function () {
    $("#cast").DataTable();
  });
</script>
@endpush
