@extends('adminlte.master')

@section('judul')
Halaman Tambah Data Cast
@endsection

@section('content')
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/cast" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" name="nama" class="form-control" id="nama" placeholder="Enter nama">
                  </div>
                  @error('nama')
                      <div class="alert alert-danger">{{ $message}}</div>
                  @enderror
                  <div class="form-group">
                    <label for="umur">Umur</label>
                    <input type="text" name="umur" class="form-control" id="umur" placeholder="umur">
                  </div>
                  @error('umur')
                    <div class="alert alert-danger">{{ $message}}</div>
                   @enderror
                  <div class="form-group">
                    <label for="bio">Biografi</label>
                    <textarea name="bio" id="" cols="bio" rows="5" class="form-control" id="bio" placeholder="bio"></textarea>
                  </div>
                  @error('bio')
                  <div class="alert alert-danger">{{ $message}}</div>
                  @enderror

                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <a href="/cast" class="btn btn-warning"><i class="fa fa-arrow-left" aria-hidden="true"></i> Kembali</a>
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save" aria-hidden="true"></i> Simpan</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->

@endsection

@push('scripts')
  <script>

  </script>
@endpush